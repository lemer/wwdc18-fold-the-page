/*:
# 📄 Fold the Page 📄
 By Olivier Lemer
 */
/*:
 In this playground, your screen is just like a sheet of paper that you can fold onto itself!
 
 ## The Concept
 
 The idea is simple! Click and hold to start folding, release when you're satisfied, and the whole universe will be reshaped!
 
 This playground plays with this idea though four small scenes that show you the mechanics and possibilities *(if you're stuck, pressing **'s'** will skip to the next puzzle, but shhhhh 🤫)*. If you want to see more, there is also YourLevel.sks where you can create your own setting!
 
 ## The Sound
 
 You might also notice that for each puzzle you complete, the music gets a little better! I created a few tracks on Garageband and used AVFoundation to have them join one by one at the perfect beat!
 
 A more subtle detail is the sound you hear as you're folding, which gets louder and higher-pitched the faster you go.
 
 
 *"Oh dear! Oh dear! I shall be too late!"* Enough of me talking, let's start playing! 😃
 
*/
import PlaygroundSupport
import SpriteKit
import AVFoundation

//: Here are the puzzles in the order they will be played. If you want to try out your own YourLevel, just add it here!
var levels: [String] = ["Introduction", "Puzzle1", "Puzzle2", "Puzzle3", "Puzzle4", "Thanks"]

/*:
 ## Thanks!
 Thank you so much for trying out my Playground! I really hope you enjoyed it, I certainly had fun making it!
 
 If you want to see even more, here comes the code!
 
 */

/*:
 
 ### The code
 
(If you want to see how the hitboxes are cut by the fold seam and the screen borders, uncomment the commented 'showPhysics' line. I think it's mezmerizing to watch)
 */

let sceneView = SKView(frame: CGRect(x:0 , y:0, width: 720, height: 540))

// sceneView.showsPhysics = true

var currentSceneName = levels[0]



/*:
 ### Prefabs
 */
let playerRadius: CGFloat = 20
let maxPlayerHorSpeed: CGFloat = 300
let maxPlayerVerSpeed: CGFloat = 320
let playerRespawnCoolDown: Double = 0.4
let mouseUpdatePeriod: Double = 0.15

let playerPrefab: SKShapeNode = {
    let prefab = SKShapeNode(circleOfRadius: playerRadius)
    prefab.name = "Player"
    prefab.zPosition = zLayer.player
    
    let body = SKPhysicsBody(circleOfRadius: playerRadius)
    body.categoryBitMask = PhysicsCategory.player
    body.collisionBitMask = PhysicsCategory.walls
    body.contactTestBitMask = PhysicsCategory.goals + PhysicsCategory.walls
    body.friction = 100.0
    body.mass = 2.0
    
    prefab.physicsBody = body
    
    // Smaller physicsBody to detect when you hit a goal
    let goalContactNode = SKNode()
    goalContactNode.name = "playerGoalTest"
    let goalContactBody = SKPhysicsBody(circleOfRadius: playerRadius / 2)
    goalContactBody.categoryBitMask = PhysicsCategory.nothing
    goalContactBody.collisionBitMask = PhysicsCategory.nothing
    goalContactBody.contactTestBitMask = PhysicsCategory.goals
    goalContactBody.affectedByGravity = false
    goalContactBody.pinned = true
    goalContactNode.position = CGPoint.zero
    goalContactNode.physicsBody = goalContactBody
    
    prefab.addChild(goalContactNode)
    
    // Shifted physicsBody that is used to know if you are standing on something
    let wallContactNode = SKNode()
    wallContactNode.name = "playerWallTest"
    let wallContactBody = SKPhysicsBody(circleOfRadius: playerRadius - 2)
    wallContactBody.categoryBitMask = PhysicsCategory.nothing
    wallContactBody.collisionBitMask = PhysicsCategory.nothing
    wallContactBody.contactTestBitMask = PhysicsCategory.walls
    wallContactBody.affectedByGravity = false
    wallContactBody.pinned = true
    wallContactNode.position = CGPoint(x: 0, y: -6)
    wallContactNode.physicsBody = wallContactBody
    
    prefab.addChild(wallContactNode)
    
    return prefab
}()

let goalPrefab: SKShapeNode = {
    let prefab = SKShapeNode(circleOfRadius: playerRadius)
    prefab.lineWidth = 5
    prefab.alpha = 0
    prefab.zPosition = zLayer.player
    
    let body = SKPhysicsBody(circleOfRadius: 15)
    body.categoryBitMask = PhysicsCategory.goals
    body.collisionBitMask = PhysicsCategory.nothing
    body.contactTestBitMask = PhysicsCategory.nothing
    body.isDynamic = false
    prefab.physicsBody = body
    
    return prefab
}()



let audioManager = AudioManager()

/*:
 ### FoldableScene
 
 The FoldableScene class represents a scene that can be folded (duh). It expects to start from a scene that already contains :
 - An SKNode named `Container`, that contains all nodes that should be foldable. SKShapeNodes in `Container` can optionally contain information in their `userData` to indicate that they are glued or deadly.
 - An SKSpriteNode named `Background` with a color
 - An SKSpriteNode named `Player` with a color and position
 
 The scene itself can also have data in its `userData` dictionary to give it some information about the level design. This includes :
 - `foldCountLimit` (Integer): The maximum number of folds allowed in this level. Can be negative if no limit must be set
 - `showFoldCountAfterFold` (Integer): The number of folds after which the UI fold counter will fade in, in the top left of the screen. Can be negative if you don't want it to show up. This is useful for the tutorial mostly
 - `restrictFirstFold` (Boolean): Indicates whether the first fold should be restricted or not. This again is useful only for the tutorial.
 
 Optionally, it will also recognize nodes named `Goal[i]`, where `[i]` is bigger or equal to zero and represents the order in which the goal should appear.
 
 In addition, it will recognize nodes whose name contains the following strings :
 - *'VisibleAfterFold[i]'*, where *[i]* is the number of folds after which the object will fade in
 - *'VisibleAfterLoad'*, for nodes that must fade in right after the scene loaded
 - *'VisibleOnLoadScreen'*, for nodes that must be visible on the load screen at the end of the puzzle
 - *'HiddenAfterMove'*, for nodes that must fade out after the first player movement
 - *'HiddenAtFold[i]'*, for nodes that must fade out after the *[i]th* fold
 */
class FoldableScene: SKScene, SKPhysicsContactDelegate {
    private var player : SKShapeNode!
    
    private var goals : [[SKNode]] = []
    
    private var canMovePlayer = true
    private var playerHorForce: CGFloat = 0
    private var playerVerForce: CGFloat = 0

    
    private var foldManager: FoldManager!

    private var foldCountContainer = SKNode()
    private var foldCount: Int = 0
    private var foldCountLimit: Int = 20
    private let foldCountUnitSize = CGSize(width: 40, height: 12)
    private let foldCountPadding: CGFloat = 16
    
    private var nextMouseUpdate = Date()
    
    
    private let clickToContinue = SKLabelNode(text: "Click to continue...")
    private var waitingToContinue: Bool = false
    
    
    // variables used for the tutorial
    private var firstFoldRestriction: FoldRestriction? = nil
    private var playerDidMove: Bool = false

    
    override func didMove(to view: SKView) {
        setupPlayer()
        setupFold()
        setupGoals()
        setupClickToContinue()
        
        if (self.userData?["restrictFirstFold"] as? Bool) ?? false {
            firstFoldRestriction = FoldRestriction(x: 0.0,
                                                   y: nil,
                                                   rotation: -CGFloat.pi / 2.0,
                                                   coordAccuracy: 20,
                                                   rotationAccuracy: CGFloat.pi / 24.0)
        }
        
        enumerateChildNodes(withName: "*VisibleAfterLoad*") { (node, _) in
            node.run(.sequence([
                .wait(forDuration: 0.7),
                .fadeIn(withDuration: 0.5)
                ]))
        }

        physicsWorld.contactDelegate = self
        physicsWorld.gravity.dy = -9

        let cameraNode = SKCameraNode()
        cameraNode.position = CGPoint.zero
        addChild(cameraNode)
        camera = cameraNode
    }
    
    
    /* * * * * * * * * * * * * * * * * * * * * * * * * * *
     
     Setup
     
     * * * * * * * * * * * * * * * * * * * * * * * * * * */
    
    func setupPlayer() {
        let position: CGPoint
        let color: SKColor
        
        if let givenPlayer = self.childNode(withName: "Player") as? SKSpriteNode {
            position = givenPlayer.position
            color = givenPlayer.color
            givenPlayer.alpha = 0
        } else {
            position = CGPoint.zero
            color = .cyan
        }
        
        player = playerPrefab.copy() as! SKShapeNode
        player.fillColor = color
        player.strokeColor = color
        player.position = position
        addChild(player)
        
        player!.alpha = 0
        player!.physicsBody?.affectedByGravity = false
        canMovePlayer = false
        player!.run(.sequence([
            .fadeIn(withDuration: 0.5),
            .run({
                self.player!.physicsBody?.affectedByGravity = true
                self.canMovePlayer = true
            })]))
        
        audioManager.attachPocs(to: player!)
    }

    
    func setupFold() {
        // Getting objects and background color from the scene
        let objectsContainer = self.childNode(withName: "Container") ?? SKNode()
        let backgroundColor = (self.childNode(withName: "Background") as? SKSpriteNode)?.color ?? .white
        
        foldManager = FoldManager(inSize: size,
                                  objectsFrom: objectsContainer,
                                  backgroundColor: backgroundColor,
                                  usingAudioFrom: audioManager,
                                  updatePeriod: mouseUpdatePeriod)
        
        let foldContainer = foldManager.container
        addChild(foldContainer)
        
        
        // Displaying the fold count
        if let showAfter = self.userData?["showFoldCountAfterFold"] as? Int {
            foldCountContainer.alpha = 0
            if showAfter > 0 {
                foldCountContainer.name = "VisibleAfterFold" + String(showAfter)
            }
        }
        foldCountLimit = (self.userData?["foldCountLimit"] as? Int) ?? -1
        
        foldCountContainer.zPosition = zLayer.UI
        foldCountContainer.removeAllChildren()
        addChild(foldCountContainer)
        
        
        var x = -size.width/2 + 30 + foldCountUnitSize.width/2
        let y = size.height/2 - 30 - foldCountUnitSize.height/2
        
        let color = player?.fillColor ?? .white
        
        for i in 0..<max(0, foldCountLimit) {
            let foldUnit = SKShapeNode(rectOf: foldCountUnitSize)
            foldUnit.fillColor = color
            foldUnit.lineWidth = 0
            foldUnit.position = CGPoint(x: x, y: y)
            foldUnit.name = String(i)
            x += foldCountUnitSize.width + foldCountPadding
            foldCountContainer.addChild(foldUnit)
        }
    }
    
    
    func setupClickToContinue() {
        clickToContinue.fontName = "Helvetica Neue Bold"
        clickToContinue.fontSize = 22.0
        clickToContinue.fontColor = .white
        clickToContinue.horizontalAlignmentMode = .right
        clickToContinue.verticalAlignmentMode = .baseline
        clickToContinue.position = CGPoint(x: size.width / 2 - 40, y: -size.height / 2 + 40)
    }
    
    
    func setupGoals() {
        var i = 0
        
        let color = player?.fillColor ?? .white
        
        var ith_goals: [SKNode] = []
        repeat {
            ith_goals = []
            let name = "Goal"+String(i)
            
            enumerateChildNodes(withName: name, using: { (node, _) in
                let copy = goalPrefab.copy() as! SKShapeNode
                copy.position = node.position
                copy.strokeColor = color
                copy.name = name
                copy.run(.fadeIn(withDuration: 0.5))
                ith_goals.append(copy)
                node.alpha = 0
            })
            if !ith_goals.isEmpty {
                goals.append(ith_goals)
            }
            i += 1
            
        } while !ith_goals.isEmpty
        
        if !goals.isEmpty {
            for goal in goals[0] {
                addChild(goal)
            }
        }
    }
    
    /**
     Removes the given goal and shows the next goal if there is one. If there is no more goals, loads the next puzzle
     
     - Parameter goal: goal to be removed. Must be in the goals array
     */
    func removeGoal(_ goal: SKNode) {
        goal.physicsBody = nil
        if let index = goals[0].index(of: goal) {
            goals[0].remove(at: index)
            if (goals[0].isEmpty) {
                goals.remove(at: 0)
                if (!goals.isEmpty) {
                    for goal in goals[0] {
                        addChild(goal)
                    }
                }
            }
        }
        goal.run(.sequence([.fadeOut(withDuration: 1.2),
                            .removeFromParent()]))
        if (goals.isEmpty) {
            levelComplete()
        }
    }
    
    
    /**
     Increments the fold count, updates the user interface, and triggers any object that must be made visible after this fold
     */
    func incrementFoldCount() {
        foldCount += 1
        if let foldUnit = foldCountContainer.childNode(withName: String(foldCountLimit - foldCount)) {
            foldUnit.run(.sequence([.fadeOut(withDuration: 0.5),
                                    .removeFromParent()]))
        }
        enumerateChildNodes(withName: "*VisibleAfterFold"+String(foldCount)+"*") { (node, _) in
            node.run(.sequence([
                .wait(forDuration: 0.7),
                .fadeIn(withDuration: 0.5)
                ]))
        }
    }
    
    
    
    /* * * * * * * * * * * * * * * * * * * * * * * * * * *
     
     Level Complete
     
     * * * * * * * * * * * * * * * * * * * * * * * * * * */
    
    /**
     Shows the 'click to continue' loading screen and schedules the next music track
     */
    func levelComplete() {
        if let background = childNode(withName: "Background")?.copy() as? SKSpriteNode {
            canMovePlayer = false
            waitingToContinue = true
            player!.physicsBody?.affectedByGravity = false
            playerHorForce = 0
            playerVerForce = 0

            let container = SKNode()
            container.alpha = 0
            container.zPosition = zLayer.loadScreen

            // Background
            background.position = CGPoint.zero
            background.zPosition = 0
            background.alpha = 1
            container.addChild(background)

            // Content from the .sks, usually labels
            enumerateChildNodes(withName: "*VisibleOnLoadScreen*") { (node, _) in
                node.alpha = 1
                node.move(toParent: container)
                node.zPosition = 1
            }
            
            // Click to continue label
            container.addChild(clickToContinue)
            clickToContinue.zPosition = 1
            
            container.run(.sequence([
                .wait(forDuration: 0.5),
                .fadeIn(withDuration: 0.5)]))
            addChild(container)
        } else {
            loadNextLevel()
        }

        audioManager.addNextMusicTrack()
    }
    
    /**
     Reloads the current level
     */
    func reloadLevel() {
        
        canMovePlayer = false
        player!.physicsBody?.affectedByGravity = false
        playerHorForce = 0
        playerVerForce = 0
        
        if let bg_copy = childNode(withName: "Background")?.copy() as? SKShapeNode {
            bg_copy.zPosition = zLayer.loadScreen
            bg_copy.alpha = 0
            bg_copy.run(.sequence([
                .fadeIn(withDuration: 0.3),
                .wait(forDuration: 0.2),
                .run {
                    self.loadLevel(fileNamed: currentSceneName)
                }
                ]))
            addChild(bg_copy)
        } else {
            self.loadLevel(fileNamed: currentSceneName)
        }
    }
    
    /**
     Loads the next level in the levels array, or Thanks.sks if there is none left
     */
    func loadNextLevel() {
        let nextSceneName: String
        if (levels.isEmpty) {
            nextSceneName = "Thanks"
        } else {
            nextSceneName = levels.removeFirst()
        }
        
        currentSceneName = "Scenes/" + nextSceneName
        loadLevel(fileNamed: currentSceneName)
    }
    
    /**
     Loads the specified level.
     - Parameter fileName: The name of the file for the level to load. It must be the full path within the Resources folder
     */
    func loadLevel(fileNamed fileName: String) {
        let transition = SKTransition.fade(withDuration: 1.2)
        
        if let nextScene = FoldableScene(fileNamed: fileName) {
            nextScene.scaleMode = .aspectFill
            
            self.scene?.removeFromParent()
            let vw = self.view
            
            vw?.presentScene(nextScene, transition: transition)
        } else {
            print("error while loading scene '\(fileName)'")
        }
    }
    
    
    
    /* * * * * * * * * * * * * * * * * * * * * * * * * * *
     
     Mouse Interaction
     
     * * * * * * * * * * * * * * * * * * * * * * * * * * */

    override func mouseDown(with event: NSEvent) {
        let position = event.location(in: self)
        if waitingToContinue {
            loadNextLevel()
        } else if !foldManager.isFolding {
            touchFoldBegan(position)
        }
    }
    
    override func mouseDragged(with event: NSEvent) {
        let newTime = Date()
        if (nextMouseUpdate.compare(newTime).rawValue < 0) {
            nextMouseUpdate = newTime.addingTimeInterval(mouseUpdatePeriod)
            let position = event.location(in: self)
            if foldManager.isFolding {
                touchFoldMoved(to: position)
            }
        }
    }
    
    override func mouseUp(with event: NSEvent) {
        let position = event.location(in: self)
        touchFoldEnded(position)
    }
    
    /**
     Called when the player wants to start a fold. Will be ignored if the fold limit has been reached
     - Parameter touchPosition: Position on screen where the player clicked to start a fold
     */
    func touchFoldBegan(_ touchPosition: CGPoint) {
        if foldCount >= foldCountLimit && foldCountLimit > 0 {
            return
        }
        
        player?.physicsBody?.isDynamic = false
        
        enumerateChildNodes(withName: "*HiddenAtFold"+String(foldCount+1)+"*") { (node, _) in
            node.run(.sequence([
                .wait(forDuration: 0.7),
                .fadeOut(withDuration: 0.5)
                ]))
        }
        
        foldManager.startFold(withTouchPosition: touchPosition, inView: sceneView)
    }
    
    /**
     Called when the player moves his mouse while folding
     - Parameter touchPosition: The new position of the mouse where the fold must be moved to.
     */
    func touchFoldMoved(to touchPosition: CGPoint) {
        foldManager.updateFold(toTouchPosition: touchPosition)
    }
    
    /**
     Called when the player releases the fold.
     - Parameter touchPosition: The final position where the fold must be validated
     */
    func touchFoldEnded(_ touchPosition: CGPoint) {
        player?.physicsBody?.isDynamic = true
        
        let restriction = (foldCount == 0) ? firstFoldRestriction : nil
        
        let success = foldManager.endFold(atTouchPosition: touchPosition, withRestriction: restriction)
        if success {
            incrementFoldCount()
        }
    }
    
    
    
    /* * * * * * * * * * * * * * * * * * * * * * * * * * *
     
     Keyboard Interaction
     
     * * * * * * * * * * * * * * * * * * * * * * * * * * */
    
    /**
     Checks whether the player is currently standing on something or not
     - Returns: True if there is floor under the player, false otherwise
     */
    func doesPlayerTouchFloor() -> Bool {
        var touchesFloor: Bool = false
        if let contactNode = player?.childNode(withName: "playerWallTest") {
            if let pb = contactNode.physicsBody {
                for other in pb.allContactedBodies() {
                    if other.node != nil && other.node!.name == "cuttable" {
                        touchesFloor = true
                        break
                    }
                }
            }
        }
        return touchesFloor
    }
    
    override func keyDown(with event: NSEvent) {
        let code = Int(event.keyCode)
        if canMovePlayer {
            switch code {
            case 125:
                playerVerForce = -maxPlayerVerSpeed
            case 126:
                if doesPlayerTouchFloor() {
                    playerVerForce = maxPlayerVerSpeed
                }
            case 124:
                playerHorForce = maxPlayerHorSpeed
            case 123:
                playerHorForce = -maxPlayerHorSpeed
            default:
                break
            }
        }
        if (event.characters?.contains("s") ?? false) {
            levelComplete()
        }
        if (code == 49) {
            if waitingToContinue {
                loadNextLevel()
            } else {
                reloadLevel()
            }
        }
    }
    
    override func keyUp(with event: NSEvent) {
        let code = Int(event.keyCode)
        switch code {
        case 125, 126:
            playerVerForce = 0
        case 124, 123:
            // I had to make it delayed because I was sometimes getting a keyDown right after the keyUp.
            run(.sequence([.wait(forDuration: 0.05),
                           .run {
                            self.playerHorForce = 0
                }]))
        default:
            break
        }
    }
    
    
    override func update(_ currentTime: TimeInterval) {
        // I move the player by modifying its velocity directly since this gives me
        // a lot more control over its behavior.
        
        if !foldManager.isFolding {
            // Triggers first move
            if (playerVerForce != 0 || playerHorForce != 0) && !playerDidMove {
                enumerateChildNodes(withName: "*HiddenAfterMove*") { (node, _) in
                    node.run(.fadeOut(withDuration: 0.5))
                }
                playerDidMove = true
            }
            
            
            if let v = player?.physicsBody?.velocity {
                var dx: CGFloat = 0
                var dy: CGFloat = v.dy
                
                // Horizontal movement
                if playerHorForce != 0 {
                    dx = min(maxPlayerHorSpeed, v.dx + (playerHorForce - v.dx)/10)
                } else {
                    dx = v.dx * 0.85
                }
                
                // Vertical movement. Did you see you can dash down ?
                if playerVerForce != 0 {
                    dy = min(v.dy + playerVerForce, maxPlayerVerSpeed)
                    playerVerForce -= playerVerForce / 3
                }
                
                player!.physicsBody!.velocity = CGVector(
                    dx: dx,
                    dy: dy)
                
                
                // Checking if player is dead
                let isPlayerDead = player != nil && player!.position.y < -size.height/2 - playerRadius * 2
                if isPlayerDead {
                    player!.removeFromParent()
                    player!.physicsBody?.affectedByGravity = false
                    player!.position = CGPoint.zero
                    run(.sequence([
                        .wait(forDuration: playerRespawnCoolDown),
                        .run({
                            self.setupPlayer()
                        })]))
                }
            }
            player?.zRotation = 0
        }
    }
    
    
    
    
    /* * * * * * * * * * * * * * * * * * * * * * * * * * *
     
     Contacts
     
     * * * * * * * * * * * * * * * * * * * * * * * * * * */
    
    func didBegin(_ contact: SKPhysicsContact) {
        if let nodeA = contact.bodyA.node,
            let nodeB = contact.bodyB.node {
            
            if (nodeA.name == "playerGoalTest" && (nodeB.name?.hasPrefix("Goal") ?? false))
            || (nodeB.name == "playerGoalTest" && (nodeA.name?.hasPrefix("Goal") ?? false)){
                
                let goal = (nodeB.name == "playerGoalTest") ? nodeA : nodeB
                removeGoal(goal)
                
            } else if ((nodeA.name == "Player") && (nodeB.name?.hasPrefix("Goal") ?? false))
                   || ((nodeB.name == "Player") && (nodeA.name?.hasPrefix("Goal") ?? false)){
                
                (nodeA.name == "Player" ? nodeB : nodeA).run(.scale(to: 1.1, duration: 0.2))
                
            } else if ((nodeB.name == "Player") && ((nodeA.userData?["isDeadly"] as? Bool ?? false)))
                   || ((nodeA.name == "Player") && ((nodeB.userData?["isDeadly"] as? Bool ?? false))) {
                
                player?.physicsBody?.isDynamic = false
                player?.run(.sequence([
                    .wait(forDuration: 0.3),
                    .fadeOut(withDuration: 0.4),
                    .run({ self.player!.position = CGPoint(x: 0, y: -self.size.height) })
                    ]))
                
            } else if (nodeA.name == "Player" && contact.bodyB.categoryBitMask == PhysicsCategory.walls)
                ||  (nodeB.name == "Player" && contact.bodyA.categoryBitMask == PhysicsCategory.walls) {
                
                audioManager.playPoc()
            }
        }
    }
    func didEnd(_ contact: SKPhysicsContact) {
        if let nodeA = contact.bodyA.node,
            let nodeB = contact.bodyB.node {
            
            if (nodeA.name == "Player") && (nodeB.name?.hasPrefix("Goal") ?? false)
            || (nodeB.name == "Player") && (nodeA.name?.hasPrefix("Goal") ?? false) {
                
                (nodeA.name == "Player" ? nodeB : nodeA).run(.scale(to: 1, duration: 0.2))
                
            }
        }
    }
    
    
    
    
}


/**
 Simple Introduction scene that shows the image explaining the basic controls, and waits for the user to click on the screen to load the first actual puzzle
 */
class IntroductionScene: SKScene {
    override func didMove(to view: SKView) {
        
        let cameraNode = SKCameraNode()
        cameraNode.position = CGPoint.zero
        addChild(cameraNode)
        camera = cameraNode
        
        
        let instructions = SKSpriteNode(imageNamed: "controlsScreen.jpg")
        instructions.size = self.size
        addChild(instructions)
        instructions.zPosition = 0
        
        
        let label = SKLabelNode(text: "Click to start !")
        label.fontName = "Helvetica Neue Bold"
        label.fontSize = 22.0
        label.fontColor = .black
        label.horizontalAlignmentMode = .right
        label.verticalAlignmentMode = .baseline
        label.position = CGPoint(x: size.width / 2 - 40, y: -size.height / 2 + 30)
        
        label.run(.repeatForever(.group([
            .customAction(withDuration: Double.pi, actionBlock: { (node, t) in
                node.position.y += sin(2 * t) * 0.2
            }),
            .sequence([
                .fadeAlpha(to: 0.5, duration: Double.pi/2.0),
                .fadeAlpha(to: 1, duration: Double.pi/2.0)
                ])
            ])))
        
        addChild(label)
        label.zPosition = 3
    }
    
    override func mouseDown(with event: NSEvent) {
        let whiteBackground = SKSpriteNode(color: .white, size: size)
        whiteBackground.alpha = 0
        whiteBackground.zPosition = 5
        whiteBackground.position = CGPoint.zero
        whiteBackground.run(.sequence([
            .fadeIn(withDuration: 0.5),
            .wait(forDuration: 0.05),
            .run {self.loadLevel()}
            ]))
        addChild(whiteBackground)
    }
    
    func loadLevel() {
        currentSceneName = "Scenes/" + levels.removeFirst()
        if let scene = FoldableScene(fileNamed: currentSceneName) {
            scene.scaleMode = .aspectFill
            
            sceneView.presentScene(scene)
            
            audioManager.startPlayingMusic()
        }
    }
}


let scene = IntroductionScene(size: sceneView.frame.size)
    scene.scaleMode = .aspectFill
    
    sceneView.presentScene(scene)


PlaygroundSupport.PlaygroundPage.current.liveView = sceneView
