import Foundation
import SpriteKit

/**
 
 FoldManager offers an API for creating, moving and finalizing a fold.
 
 A very basic idea of how FoldManager works is in two parts. When the fold is first started, a 'screenshot' of the foldable nodes is taken and used for the visual part of the fold.
 On the other hand, in order for the physicsbodies of the blocks to be accurate, they have to be recomputed when the fold is validated. This allows for this big computation to be done only once per fold, while keeping the folding animation nice and smooth !
 
 During a fold, the node hierarchy is as follows :
 - container (SKNode)
   - captureContainer (SKNode): Contains all nodes that must be folded. For example, this does not include gray blocks
     - cuttableContainer (SKNode): Contains all nodes that come with an SKPhysicsBody
     - mirrorableContainer (SKNode): Contains all nodes that must be folded visually
   - fold (SKNode): This contains everything related to the fold and is only here during a fold
     - foldMask (SKCropNode): This will contain the folded page, cropped to hide what is beyond the fold seam
       - foldWrapper (SKNode): Since foldMask will be rotated differently than the folded page itself, foldWrapper will compensate for that.
 

 */
public class FoldManager {
    private let foldCancelDistance: CGFloat = 20

    private let size : CGSize
    private let audioManager: AudioManager
    private let updatePeriod: Double

    
    public let container = SKNode()
    private let captureContainer = SKNode()
    private let mirrorableContainer: SKNode
    private let cuttableContainer = SKNode()
    
    
    private let fold = SKNode()
    private let foldBackground : SKShapeNode
    private let foldLine : SKShapeNode
    private let foldMask = SKCropNode()
    private var foldWrapper = SKNode()
    private var foldCancelCircle : SKShapeNode?

    private var cuttables : [CuttableShape] = []

    
    private var touchStartPoint : CGPoint? = nil
    private var prevFoldRotation : CGFloat = 0.0
    private var foldWasValid: Bool = false
    
    public private(set) var isFolding : Bool = false
    
    
    public init(inSize size: CGSize,
                objectsFrom mirrorableContainer: SKNode,
                backgroundColor bg_color: NSColor,
                usingAudioFrom audioManager: AudioManager,
                updatePeriod period: Double) {
        
        self.mirrorableContainer = mirrorableContainer

        self.updatePeriod = period
        self.audioManager = audioManager
        self.size = size

        
        // Create the background used when folding
        self.foldBackground = SKShapeNode(rectOf: CGSize(width: size.width, height: size.height))
        foldBackground.lineWidth = 0
        foldBackground.fillColor = bg_color
        foldBackground.alpha = 0

        // Create the white line visible during fold
        var line_points = [CGPoint(x: -size.width, y: 0),
                           CGPoint(x:  size.width, y: 0)]
        foldLine = SKShapeNode(points: &line_points, count: 2)
        fold.addChild(foldLine)

        // Setup foldMask
        let foldMask_rect = SKShapeNode(rectOf: CGSize(width: size.width * 2, height: size.width))
        foldMask_rect.fillColor = .white
        foldMask_rect.position = CGPoint(x: 0, y: size.width / 2)

        self.foldMask.maskNode = foldMask_rect
        self.foldMask.yScale = -1;

        fold.addChild(foldMask)

        // Setup main container
        setupContainer()
    }
    
    private func setupContainer() {
        self.container.addChild(self.captureContainer)
        self.captureContainer.addChild(self.cuttableContainer)
        self.mirrorableContainer.move(toParent: self.captureContainer)
        
        self.fold.zPosition = zLayer.fold
        self.mirrorableContainer.zPosition = 2
        self.cuttableContainer.zPosition = 1
        
        for child in mirrorableContainer.children {
            // If child is a label, just make it live on top of all other blocks
            if child.name?.contains("Label") ?? false {
                child.zPosition = 1
                continue
            }
            
            let isDeadly = (child.userData?["isDeadly"] as? Bool) ?? false
            let isGlued = (child.userData?["isGlued"] as? Bool) ?? false
            
            // Creating a PhysicsBody for the shape
            let cRotation = child.zRotation
            child.zRotation = 0
            var rectSize = CGSize(width: child.frame.width, height: child.frame.height)
            if isDeadly {
                rectSize = CGSize(width: child.frame.width - 5, height: child.frame.height - 5)
            }
            child.zRotation = cRotation
            
            let body = SKPhysicsBody(rectangleOf: rectSize)
            body.isDynamic = false
            body.categoryBitMask = PhysicsCategory.walls
            body.collisionBitMask = PhysicsCategory.player
            child.physicsBody = body
            child.name = "cuttable"

            // if child is glued, do not create cuttable from it and move it out of mirrorableContainer
            if isGlued {
                child.move(toParent: container)
                child.zPosition = 0
            } else if let shape = child as? SKShapeNode {
                // I'm feeling generous so I'm reducing the hitbox of deadly blocks ;)
                if isDeadly {
                    let copy = shape.copy() as! SKShapeNode

                    copy.xScale -= 0.05
                    copy.yScale -= 0.05

                    cuttables.append(CuttableShape(from: copy))
                } else {
                    cuttables.append(CuttableShape(from: shape))
                }
                shape.move(toParent: cuttableContainer)
            }
        }
    }
    
    /**
     Starts a fold at the provided touch position using the provided view
     
     - Parameter touchPosition: touch position at which the fold is to be started
     - Parameter view: view SKView to be used to generate the texture of the fold
     */
    public func startFold(withTouchPosition touchPosition: CGPoint, inView view: SKView) {
        isFolding = true
        touchStartPoint = touchPosition
        
        // Setup fold nodes
        fold.removeAllActions()
        foldWrapper.removeAllActions()
        
        fold.position = touchPosition
        fold.zRotation = 0.0
        fold.removeFromParent()
        container.addChild(fold)
        
        foldMask.zRotation = 0.0
        foldMask.position = CGPoint.zero

        foldWrapper = SKNode()
        foldMask.addChild(foldWrapper)
        
        // Taking capture of captureContainer
        let captureRect = CGRect(x: -size.width / 2,
                   y: -size.height / 2,
                   width: size.width,
                   height: size.height)
        let tex = view.texture(from: self.captureContainer,
                                    crop: captureRect)
        
        // changing the contents of mirrorable to be the new capture
        let newMirrorable = SKSpriteNode(texture: tex)
        self.mirrorableContainer.removeAllChildren()
        self.mirrorableContainer.addChild(newMirrorable)
        
        // Also adding the capture in foldWrapper with a white-ish background for the looks!
        let captureToFold = newMirrorable.copy() as! SKSpriteNode
        self.foldWrapper.addChild(captureToFold)
        if let white_bg = foldBackground.copy() as? SKShapeNode {
            captureToFold.zPosition = 1
            white_bg.alpha = 0.17
            white_bg.fillColor = foldBackground.fillColor.blended(withFraction: 0.6, of: .white) ?? .white
            self.foldWrapper.addChild(white_bg)
        }
        
        // Fold is not valid yet, make it invisible
        fold.alpha = 0
        foldWasValid = false
        
        // Setup the black cancel circle
        foldCancelCircle = SKShapeNode(circleOfRadius: foldCancelDistance)
        foldCancelCircle!.lineWidth = 5
        foldCancelCircle!.strokeColor = .black
        foldCancelCircle!.zPosition = zLayer.UI
        foldCancelCircle!.position = touchStartPoint!
        container.addChild(foldCancelCircle!)
        
    }
    
    /**
     Updates the currently existing fold's position
     
     - Parameter touchPosition: touch position at which the fold must be moved
     
     - Returns: A boolean indicating whether fold is still going (true) or if it was canceled (false)
     */
    public func updateFold(toTouchPosition touchPosition: CGPoint) -> Bool {
        if !isFolding {
            return false
        }
        
        let (nextFoldPos, nextFoldRot) = getBisection(touchStartPoint!, touchPosition)
        let nextWrapperPosition = foldWrapperPositionFor(foldPosition: nextFoldPos, foldRotation: nextFoldRot)
        
        let foldDistance = touchStartPoint!.distanceTo(nextFoldPos)
        let foldIsValid = foldDistance > foldCancelDistance
        
        // Correcting rotation if around +/- PI
        let rotationCorrection: CGFloat
        if (abs(nextFoldRot - prevFoldRotation) > CGFloat.pi) {
            if (nextFoldRot < 0) {
                rotationCorrection = -2 * CGFloat.pi
            } else {
                rotationCorrection =  2 * CGFloat.pi
            }
        } else {
            rotationCorrection = 0
        }
        
        // updating fold sound
        if foldIsValid && foldWasValid {
            audioManager.foldSpeed = min(1.0, Float(nextFoldPos.distanceTo(fold.position)) / 50.0 - 0.1)
        }
        
        // If fold is canceled
        if !foldIsValid && foldWasValid {
            cancelFold(atNewPosition: nextFoldPos, withNewRotation: nextFoldRot)
            self.isFolding = false
            return false
        }

        // Fold Animation
        let duration: Double
        let key: String
        
        
        // When the fold becomes valid it slides slower since it comes from farther away, so we give
        // the longer animation the "toggle" key. That way, we can make sure we let the "toggle"
        // animation finish before animating normally
        
        if (foldIsValid) {
            // if fold becomes valid or if it is already valid and the toggle animation is done, animate
            if (!foldWasValid || fold.action(forKey: "toggle") == nil) {
                if (fold.hasActions() || foldWrapper.hasActions()) {
                    fold.removeAllActions()
                    foldWrapper.removeAllActions()
                }
                fold.zRotation += rotationCorrection
                foldWrapper.zRotation -= rotationCorrection
                
                // If fold starts being valid, we need to change its position and rotation
                // directly before letting it slide in, otherwise it would slide
                // in from its previous 'out-of-frame' position, which would look incorrect
                if (!foldWasValid) {
                    duration = 0.8
                    key = "toggle"
                    
                    fold.alpha = 1
                    let line_pos = offScreenFoldPosition(foldPosition: nextFoldPos)
                    let foldWrapperPosition = foldWrapperPositionFor(foldPosition: line_pos, foldRotation: nextFoldRot)
                    
                    fold.position = line_pos
                    fold.zRotation = nextFoldRot
                    foldWrapper.position = foldWrapperPosition
                    foldWrapper.zRotation = -nextFoldRot
                    
                    container.run(.sequence([.wait(forDuration: duration), .run {
                        self.audioManager.startFoldAudio()
                        self.audioManager.foldSpeed = 0
                        } ]))
                    
                } else {
                    duration = updatePeriod
                    key = "normal"
                }
                
                // Running the animations
                fold.run(.group([
                    .move(to: nextFoldPos, duration: duration),
                    .rotate(toAngle: nextFoldRot, duration: duration)]),
                          withKey: key)
                
                foldWrapper.run(.group([
                    .move(to: nextWrapperPosition, duration: duration),
                    .rotate(toAngle: -nextFoldRot, duration: duration)]),
                                 withKey: key)
                
            }
        } else {
            // If fold is not valid, no need to animate anything
            fold.position = nextFoldPos
            fold.zRotation = nextFoldRot
            foldWrapper.position = nextWrapperPosition
            foldWrapper.zRotation = -nextFoldRot
        }
        
        // Updating cancel circle
        let fold_cancel_factor = 2.0 * foldDistance / size.width
        foldCancelCircle!.setScale(1.0 + fold_cancel_factor)
        foldCancelCircle!.alpha = 1.0 / max(0.01, 1 + fold_cancel_factor)
        
        // And we're done!
        foldWasValid = foldIsValid
        prevFoldRotation = nextFoldRot
        
        return isFolding
    }
    
    /**
     Computes the position of foldWrapper for the given fold position and rotation
     
     - Parameter foldPosition: Position of the fold
     - Parameter foldRotation: Rotation of the fold
     
     - Returns: The position that foldWrapper should have for the provided fold parameters
     */
    private func foldWrapperPositionFor(foldPosition: CGPoint, foldRotation phi: CGFloat) -> CGPoint {
        let beta = CGVector(dx: 0, dy: -1).angle(with: (-foldPosition).vector())
            * (foldPosition.x > 0 ? -1 : 1)
            - (phi - CGFloat.pi / 2)
        let mag = foldPosition.magnitude()
        return CGPoint(x: -mag * cos(beta), y: -mag * sin(beta))
    }
    
    /**
     Computes the fold position if we want it to be off-screen but still oriented correctly
     
     - Parameter foldPosition: Position of the fold on screen
     
     - Returns: The position of the fold off screen
     */
    private func offScreenFoldPosition(foldPosition: CGPoint) -> CGPoint {
        if let startPoint = touchStartPoint {
            let direction = (startPoint - foldPosition).normalize()
            let difference = direction * (size.width / 2.0 + size.height / 2.0 + foldPosition.magnitude())
            return foldPosition + difference
        }
        return foldPosition
    }
    
    /**
     Cancels the current fold by letting it slide back out of screen and remove itself from its parent
     
     - Parameter newFoldPos: Position of the fold from which to start the cancel animation
     - Parameter newFoldRot: Rotation of the fold from which to start the cancel animation
     */
    func cancelFold(atNewPosition newFoldPos: CGPoint, withNewRotation newFoldRot: CGFloat) {

        // Similarly to updateFold, we need to first force the position on screen before sliding off screen,
        // otherwise the animation will be off-center.
        let foldWrapperPosition = foldWrapperPositionFor(foldPosition: newFoldPos, foldRotation: newFoldRot)
        fold.position = newFoldPos
        fold.zRotation = newFoldRot
        foldWrapper.position = foldWrapperPosition
        foldWrapper.zRotation = -newFoldRot
        
        let duration: Double = 0.8
        
        let newFoldPosition = offScreenFoldPosition(foldPosition: newFoldPos)
        let nextWrapperPosition = foldWrapperPositionFor(foldPosition: newFoldPosition, foldRotation: newFoldRot)
        
        
        fold.removeAllActions()
        foldWrapper.removeAllActions()
        
        fold.run(.sequence([
            .group([
                .move(to: newFoldPosition, duration: duration),
                .rotate(toAngle: newFoldRot, duration: duration)]),
            .removeFromParent()]))
        
        foldWrapper.run(.sequence([
            .group([
                .move(to: nextWrapperPosition, duration: duration),
                .rotate(toAngle: -newFoldRot, duration: duration)]),
            .removeFromParent()]))

        foldCancelCircle!.run(.sequence([.group([.fadeOut(withDuration: 0.3),
                                                 .scale(to: 0, duration: 0.3)]),
                                         .removeFromParent()]))

        audioManager.stopFoldAudio()
        foldWasValid = false
    }
    
    /**
     Ends the current fold at the given position, and makes it physically existing
     
     - Parameter touchPosition: Position of the final fold
     - Parameter restriction: Restrictions that the fold is subject to
     
     - Returns: true if the fold was successful, false if it had been canceled or if it did not satisfy its restrictions
     */
    public func endFold(atTouchPosition touchPosition: CGPoint, withRestriction restriction: FoldRestriction?) -> Bool {
        fold.removeAllActions()
        foldWrapper.removeAllActions()
        
        foldCancelCircle?.run(.sequence([.group([.fadeOut(withDuration: 0.3),
                                                   .scale(to: 2.5, duration: 0.3)]),
                                           .removeFromParent()]))
        
        if !isFolding || !foldWasValid {
            isFolding = false
            foldMask.removeAllChildren()
            return false
        }
        
        isFolding = false
        
        audioManager.stopFoldAudio()

        // Checking restrictions
        let foldPosition = fold.position
        let phi = fold.zRotation
        if let restriction = restriction {
            if restriction.isSatisfiedBy(x: foldPosition.x, y: foldPosition.y, rotation: phi) {
                cancelFold(atNewPosition: fold.position, withNewRotation: fold.zRotation)
                return false
            }
        }
        
        // Letting the seam line fade out
        let lineCopy = foldLine.copy() as! SKNode
        fold.addChild(lineCopy)
        lineCopy.move(toParent: container)
        lineCopy.zRotation = fold.zRotation
        lineCopy.zPosition = 2
        lineCopy.run(.sequence([
            .fadeOut(withDuration: 0.5),
            .removeFromParent()
            ]))

        
        // Cutting all objects from cuttables
        var new_cuttables: [CuttableShape] = []
        for cuttable in cuttables {
            if let (sliced_node, sliced_obst) = cuttable.getCut(linePosition: fold.position,
                                                                   angle: fold.zRotation,
                                                                   within: size) {
                cuttableContainer.addChild(sliced_node)
                new_cuttables.append(sliced_obst)
            }
        }
        cuttables.append(contentsOf: new_cuttables)
        
        // Adding a copy of the cropped fold to mirrorableContainer for it to be visible during next fold
        let copy = foldMask.copy() as! SKCropNode
        fold.addChild(copy)
        copy.move(toParent: mirrorableContainer)
        copy.zRotation = fold.zRotation

        foldMask.removeAllChildren()
        
        fold.removeFromParent()
        
        return true
    }
    
    /**
     Computes the position and the rotation of a line representing the bisection of the two provided points
     
     - Parameter p1: first point
     - Parameter p2: second point
     
     - Returns: A (CGPoint, CGFloat) pair where the point is the position and the float is the rotation of the bisection
     */
    private func getBisection(_ p1: CGPoint, _ p2: CGPoint) -> (CGPoint, CGFloat) {
        let middle = (p1 + p2) * 0.5
        let diff = p2 - p1
        let rot = CGVector(dx: 0, dy: -1).angle(with: diff.vector()) * (
            diff.x < 0 ? -1 : 1)
        return (middle, rot)
    }
}


/**
 Represents the restrictions that one can put on a fold's position and rotation.
 */
public struct FoldRestriction {
    let x: CGFloat?
    let y: CGFloat?
    let rotation: CGFloat?
    let coordAccuracy: CGFloat
    let rotationAccuracy: CGFloat
    
    public init(x: CGFloat?, y: CGFloat?, rotation: CGFloat?, coordAccuracy: CGFloat, rotationAccuracy: CGFloat) {
        self.x = x
        self.y = y
        self.rotation = rotation
        self.coordAccuracy = coordAccuracy
        self.rotationAccuracy = rotationAccuracy
    }
    
    /**
     Checks whether the provided parameters satisfy this restriction
     */
    public func isSatisfiedBy(x: CGFloat, y: CGFloat, rotation: CGFloat) -> Bool {
        return (self.x != nil && (abs(x - self.x!) > self.coordAccuracy))
            || (self.y != nil && (abs(y - self.y!) > self.coordAccuracy))
            || (self.rotation != nil && (abs(rotation - self.rotation!) > self.rotationAccuracy))
    }
}
