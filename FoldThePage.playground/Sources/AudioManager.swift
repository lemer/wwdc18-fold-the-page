import Foundation
import AVFoundation
import SpriteKit

/**
 Represents all the information about a music track required by the AudioManager to know how and when to insert the next track into the music. All durations and lengths are expressed as 'measures' for convenience
 */
struct TrackInfo {
    let canJoinAt: Int // at which measure a track is allowed to join in
    let fadeInDuration: Double
    let length: Int
    let volume: Float
}

/**
 Used to manage everything that is sound related in the playground. This includes the music, the sound of the paper folding,
 and the 'poc's of the ball. I had a lot of fun being there rubbing paper against cloth and hitting against my phone to get those sounds :)
 */
public class AudioManager {
    // Music
    let trackInfos = [TrackInfo(canJoinAt: 0, fadeInDuration: 0.5, length: 4, volume: 0.2),
                      TrackInfo(canJoinAt: 2, fadeInDuration: 0.1, length: 1, volume: 0.25),
                      TrackInfo(canJoinAt: 4, fadeInDuration: 1.0, length: 4, volume: 0.25),
                      TrackInfo(canJoinAt: 4, fadeInDuration: 0.0, length: 20, volume: 0.35),
                      TrackInfo(canJoinAt: 4, fadeInDuration: 0.0, length: 20, volume: 0.3)]
    var trackPlayers: [AVAudioPlayer]?
    var nextTrackIndex: Int = 0
    let unitDuration: Double!

    // Pocs
    var pocAudioNodes: [SKAudioNode]

    // Fold
    public let engine = AVAudioEngine()
    public let foldAudioPlayer = AVAudioPlayerNode()
    public let pitchEffect = AVAudioUnitTimePitch()
    private var foldBuffer: AVAudioPCMBuffer? = nil
    private var endBuffer: AVAudioPCMBuffer? = nil
    
    public var foldSpeed: Float {
        set {
            self.pitchEffect.pitch = newValue * 1200 - 500
            self.foldAudioPlayer.volume = newValue * 2
        }
        get {
            return self.foldAudioPlayer.volume / 2
        }
    }
    
    public init() {
        // Music
        trackPlayers = []
        for i in 1...5 {
            if let path = Bundle.main.path(forResource: "music_track"+String(i), ofType: "mp3", inDirectory: "Sounds/Music"),
                let trackPlayer = try? AVAudioPlayer(contentsOf: URL(fileURLWithPath: path)) {
                
                trackPlayer.numberOfLoops = -1
                trackPlayers!.append(trackPlayer)
                
            } else {
                trackPlayers = nil
                break
            }
        }
        if let players = trackPlayers {
            unitDuration = players[0].duration / Double(trackInfos[0].length)
        } else {
            unitDuration = nil
        }

        // Pocs
        self.pocAudioNodes = []
        for i in 1...4 {
            if let path = Bundle.main.path(forResource: "poc"+String(i), ofType: "aiff", inDirectory: "Sounds/Pocs") {
                let audioNode = SKAudioNode(url: URL(fileURLWithPath: path))
                audioNode.autoplayLooped = false
                pocAudioNodes.append(audioNode)
            }
        }
        
        // Fold
        do {
            if let pathFold = Bundle.main.path(forResource: "foldAudio", ofType: "aiff", inDirectory: "Sounds/Fold"),
                let pathEnd = Bundle.main.path(forResource: "foldEnd", ofType: "aiff", inDirectory: "Sounds/Fold"){
                
                let foldAudioFile = try AVAudioFile(forReading: URL(fileURLWithPath: pathFold))
                let endAudioFile = try AVAudioFile(forReading: URL(fileURLWithPath: pathEnd))
                
                foldBuffer = AVAudioPCMBuffer(pcmFormat: foldAudioFile.processingFormat, frameCapacity: UInt32(foldAudioFile.length))
                endBuffer = AVAudioPCMBuffer(pcmFormat: endAudioFile.processingFormat, frameCapacity: UInt32(endAudioFile.length))
                
                if foldBuffer != nil && endBuffer != nil {
                    try foldAudioFile.read(into: foldBuffer!)
                    try endAudioFile.read(into: endBuffer!)
                    
                    self.engine.attach(self.foldAudioPlayer)
                    self.engine.attach(self.pitchEffect)
                    
                    self.engine.connect(self.foldAudioPlayer, to: self.pitchEffect, format: foldBuffer!.format)
                    self.engine.connect(self.pitchEffect, to: self.engine.mainMixerNode, format: self.pitchEffect.outputFormat(forBus: 0))
                    
                    self.engine.prepare()
                    try self.engine.start()
                }
            }
        } catch {
            print("Error while creating fold sound")
        }
        
    }
    
    /**
     Attaches the poc sounds to the given node for them to sound like they come from it
     */
    public func attachPocs(to node: SKNode) {
        for poc in pocAudioNodes {
            poc.removeFromParent()
            node.addChild(poc)
        }
    }

    /**
     Plays a random poc from the set of pocs available :)
     */
    public func playPoc() {
        let index = Int(arc4random_uniform(UInt32(pocAudioNodes.count)))
        if index == 0 {
            return
        }
        let pocAudio = pocAudioNodes[index]
        pocAudio.run(.sequence([.stop(), .play()]))
    }
    
    /**
     Starts playing the fold audio
     */
    public func startFoldAudio() {
        if foldBuffer != nil {
            foldAudioPlayer.scheduleBuffer(foldBuffer!, at: nil, options: .loops)
            foldAudioPlayer.play()
        }
    }
    
    /**
     Stops the folding audio with a 'swwwip' sound
     */
    public func stopFoldAudio() {
        if foldAudioPlayer.isPlaying {
            if endBuffer != nil {
                foldAudioPlayer.volume = 0.2
                foldAudioPlayer.scheduleBuffer(endBuffer!, at: nil, options: .interrupts)
            }
        }
    }
    
    /**
     Starts playing the first track of music
     */
    public func startPlayingMusic() {
        if let firstTrack = trackPlayers?[0], nextTrackIndex == 0 {
            firstTrack.volume = 0.0
            firstTrack.play()
            firstTrack.setVolume(trackInfos[0].volume, fadeDuration: trackInfos[0].fadeInDuration * unitDuration)
            nextTrackIndex += 1
        } else {
            print("Impossible to start first track")
        }
    }
    
    /**
     Schedules the next track of music to be added as soon as possible, with respect to its requirements specified in trackInfos
     */
    public func addNextMusicTrack() {
        if nextTrackIndex >= trackInfos.count {
            return
        }
        if let nextTrack = trackPlayers?[nextTrackIndex] {
            let currentTime = trackPlayers![0].currentTime
            let deviceCurrentTime = trackPlayers![0].deviceCurrentTime
            
            let info = trackInfos[nextTrackIndex]
            
            let maxOffset: Double = Double(info.canJoinAt) * unitDuration
            var offset: Double = maxOffset - currentTime.truncatingRemainder(dividingBy: maxOffset)
            if offset < 0.5 {
                offset += maxOffset
            }
            
            nextTrack.volume = 0.0
            nextTrack.play(atTime: deviceCurrentTime + offset)
            nextTrack.setVolume(info.volume, fadeDuration: info.fadeInDuration * unitDuration)
            
            nextTrackIndex += 1
        }
    }
    //-
}

