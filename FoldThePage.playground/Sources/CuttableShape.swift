import Foundation
import SpriteKit

/**
 Represents a ploygon that can be easily cut and flipped along a line and cropped to fit a rectangle

 The way this class is used is an instance is first created from an SKShapeNode or a sequence of points. Then the getSliced function is called, which cuts the shape along a line and all four sides of the screen, while flipping the whole shape with respect to the line.
 
 The use of this class is required in order to have a folded shape with a valid SKPhysicsBody, since it's impossible to simply call .yScale = -1 on the physicsBody of a shape, or to crop in using an SKCropNode.
 
 This is the most complicated part of this playground, and also the one that took the most time. But it was also the most interesting !
 */
public class CuttableShape {
    
    // The set of points that represent the shape
    public var points: [CGPoint]

    // Data taken from the node that was used to create the cuttable shape,
    // and will be used when generating a new, cut shape
    public var name: String = ""
    public var userData: NSMutableDictionary?
    
    
    public init(from points: [CGPoint], name: String, userData: NSMutableDictionary?) {
        self.points = points
        self.name = name
        self.userData = userData
    }
    
    public init(from shape: SKShapeNode) {
        let position = shape.position
        let angle = shape.zRotation
        var pts: [CGPoint] = []
        var prev_point: CGPoint? = nil
        
        let xScale = shape.xScale
        let yScale = shape.yScale

        // get each point in the shape's path
        shape.path?.applyWithBlock({ (elem) in
            let elem_pt = elem.pointee.points[0]
            let scaled_pt = CGPoint(x: elem_pt.x * xScale,
                                    y: elem_pt.y * yScale)
            let new_pt = CGPoint(x: scaled_pt.x * cos(angle) - scaled_pt.y * sin(angle),
                                 y: scaled_pt.x * sin(angle) + scaled_pt.y * cos(angle))
            
            if prev_point == nil || new_pt != prev_point {
                prev_point = new_pt
                pts.append(new_pt + position)
            }
        })
        
        self.userData = shape.userData
        self.name = shape.name ?? ""
        self.points = pts
    }
    
    /**
     Computes the result of cutting the shape with the given line and the given borders, while
     also flipping it around the given line.
     
     - Parameter linePosition: the position of the line to cut through and flip around
     - Parameter angle: the angle of the line to cut through
     - Parameter size: the dimensions of the current view, used to retrieve the position of the borders along which to cut the shape
     
     - Returns: An (SKNode, CuttableShape) pair corresponding to this shape after cutting and flipping it. The SKNode has a physicsbody of the correct shape.
     */
    public func getCut(linePosition: CGPoint, angle: CGFloat, within size: CGSize) -> (SKNode, CuttableShape)? {
        // Write the given line as y = a * x + b for convenience
        let p1 = linePosition
        let p2 = CGPoint(x: linePosition.x + cos(angle),
                         y: linePosition.y + sin(angle))
        let (a1, b1) = parametricLine(p1, p2);
        
        
        var new_points: [CGPoint] = []
        var path = CGMutablePath()
        
        // We will need to compare the current point's position with the previous one
        // in the loop to see if one is inside and the other is outside
        var prev_p: CGPoint? = nil
        var prev_above = true
        var prev_clipped_l = false
        var prev_clipped_r = false
        var prev_clipped_b = false
        var prev_clipped_t = false
        var potential_corner: CGPoint?
        
        let w2 = size.width / 2
        let h2 = size.height / 2

        // Creating an array that ends with its first point in order to consider every possible pair of successive points.
        var points_loop = self.points
        
        // If not enough points available, returning
        if (self.points.count > 2) {
            points_loop.append(self.points[0])
        } else {
            return nil
        }
        
        // Computing constants that are going to be used multiple times later for efficiency
        let a = -linePosition.x
        let b = -linePosition.y
        let alpha = -angle
        let cos2 = cos(2 * alpha)
        let sin2 = sin(2 * alpha)
        let tx =  a * cos2 - b * sin2 - a
        let ty = -a * sin2 - b * cos2 - b
        func flipPoint(_ p: CGPoint) -> CGPoint {
            return CGPoint(
                x:  p.x * cos2 - p.y * sin2 + tx,
                y: -p.x * sin2 - p.y * cos2 + ty)
        }
        
        // whether we keep the points that are above or below the line
        let keep_above = p1.x > p2.x || (p1.x == p2.x && p2.y < p1.y)
        
        var i = 1
        var prev_i = 0
        var flipped_pt = flipPoint(points_loop[0])
        
        while i <= points_loop.count {
            
            let px = flipped_pt.x
            let py = flipped_pt.y
            
            // testing all possible directions it can be cut through
            let limit_py = px * a1 + b1
            let this_above = limit_py >= py || (p1.x == p2.x && px > p1.x)
            let this_clipped_l = px < -w2
            let this_clipped_r = px > w2
            let this_clipped_b = py < -h2
            let this_clipped_t = py > h2
            var this_clipped = this_clipped_l || this_clipped_r || this_clipped_b || this_clipped_t
            
            
            if (prev_p != nil && prev_p != flipped_pt) {
                var x: CGFloat = 0, y: CGFloat = 0
                
                // First testing window borders and computing the point on the edge if necessary
                if (this_clipped_l != prev_clipped_l && flipped_pt.x != -w2 && prev_p!.x != -w2) {
                    let factor = abs((px + w2) / (prev_p!.x - px))
                    y = factor * (prev_p!.y - py) + py
                    x = -w2
                } else if (this_clipped_r != prev_clipped_r && flipped_pt.x != w2 && prev_p!.x != w2) {
                    let factor = abs((px - w2) / (prev_p!.x - px))
                    y = factor * (prev_p!.y - py) + py
                    x = w2
                } else if (this_clipped_t != prev_clipped_t && flipped_pt.y != h2 && prev_p!.y != h2) {
                    let factor = abs((py - h2) / (prev_p!.y - py))
                    x = factor * (prev_p!.x - px) + px
                    y = h2
                } else if (this_clipped_b != prev_clipped_b && flipped_pt.y != -h2 && prev_p!.y != -h2) {
                    let factor = abs((py + h2) / (prev_p!.y - py))
                    x = factor * (prev_p!.x - px) + px
                    y = -h2
                }
                // If it got cut, restart the loop with this new point, since there might be
                // another cut in between
                if (x != 0 || y != 0) {
                    flipped_pt = CGPoint (x: x, y: y)
                    i = prev_i
                    continue
                }
                
                // One particularly interesting configuration is when a node is cut by the corner
                // of the screen, because we need to add a point to the shape at that corner.
                // If we are in this situation, save this point as a potential corner
                if (   (px <= -w2 || px >= w2)
                    && (py <= -h2 || py >= h2)
                    && potential_corner == nil) {
                    flipped_pt = CGPoint(x: (px < 0 ? -w2 : w2),
                                         y: (py < 0 ? -h2 : h2))
                    if (prev_p!.x != flipped_pt.x || prev_p!.y != flipped_pt.y) {
                        this_clipped = false
                        potential_corner = flipped_pt
                    }
                }
                
                // If we crossed the fold seam
                if (this_above != prev_above) {
                    // If both points are on the same x, the intersection is easy to find
                    if (prev_p!.x == px) {
                        x = px
                        y = limit_py
                    } else {
                        // otherwise, compute the parametric form of the line between
                        // the two points and find the intersection
                        let (a2, b2) = parametricLine(prev_p!, flipped_pt)
                        if (a1 == a2) {
                            x = flipped_pt.x
                            y = flipped_pt.y
                        } else {
                            x = (b2 - b1) / (a1 - a2)
                            y = a2 * x + b2
                        }
                    }
                    // Finally, add this intersection point to the final points
                    let new_point = CGPoint(x: x, y: y)
                    if (new_points.count == 0) {
                        path.move(to: new_point)
                    } else {
                        path.addLine(to: new_point)
                    }
                    new_points.append(new_point)
                }
            }
            // If flipped_pt is on screen and above the fold seam, add it to the final points
            if (!this_clipped && (this_above != keep_above) && flipped_pt != prev_p) {
                if (new_points.count == 0) {
                    path.move(to: flipped_pt)
                } else {
                    path.addLine(to: flipped_pt)
                }
                new_points.append(flipped_pt)
            }
            
            prev_p = flipped_pt
            prev_above = this_above
            prev_clipped_l = this_clipped_l
            prev_clipped_r = this_clipped_r
            prev_clipped_b = this_clipped_b
            prev_clipped_t = this_clipped_t
            
            if (i >= points_loop.count) {
                break
            }
            flipped_pt = flipPoint(points_loop[i])
            prev_i = i
            i += 1
        }
        
        // Here I deal with that corner problem... In case I have added a potential corner that was
        // actually not, I need to remove it. Because, in order to know whether the corner is valid,
        // I need to know which of all the other points are also valid, it is much more efficient to
        // do it now instead of in the loop above.
        if let corner = potential_corner,
            let index = new_points.index(of: corner) {
            
            let np_count = new_points.count
            if np_count >= 3 {
                
                // Getting the two points surrounding the potential corner,
                // and checking to see if they lie on the same border of the screen or not.
                // If they do, it means the corner is invalid. Otherwise we keep it.
                let p0 = new_points[(index + np_count - 1) % np_count]
                let p1 = new_points[(index + 1) % np_count]
                if (p0.x == p1.x && p1.x == corner.x)
                    || (p0.y == p1.y && p1.y == corner.y) {

                    new_points.remove(at: index)
                    
                    // If we needed to remove it, we must recompute the path.
                    // This is slightly inefficient, but on average it makes the entire thing MUCH faster!
                    path = CGMutablePath()
                    for (i, point) in new_points.enumerated() {
                        if i == 0 {
                            path.move(to: point)
                        } else {
                            path.addLine(to: point)
                        }
                    }
                }
            }
        }
        
        path.closeSubpath()

        // If we found more than 2 valid points, then this shape is worth existing!
        if (new_points.count > 2) {
            let new_node = SKNode()
            // I am using edgeLoopFrom and not polygonFrom, because polygonFrom
            // does not accept shapes that were too thin.
            let body = SKPhysicsBody(edgeLoopFrom: path)
            body.isDynamic = false
            body.categoryBitMask = PhysicsCategory.walls
            body.collisionBitMask = PhysicsCategory.player
            body.contactTestBitMask = PhysicsCategory.nothing
            new_node.physicsBody = body

            new_node.name = self.name
            new_node.userData = self.userData
            
            let new_obst = CuttableShape(from: new_points, name: self.name, userData: self.userData)

            return (new_node, new_obst)
            
        } else {
            return nil
        }
    }
    
    /**
     Returns the values of the parametric representation of the line
     
     - Parameter p1: first point of the line
     - Parameter p2: second point on the line
     
     - Returns: A pair of CGFloats (a, b), where the provided line's representation is y = a * x + b
     */
    private func parametricLine(_ p1: CGPoint, _ p2: CGPoint) -> (CGFloat, CGFloat) {
        let a1 = (p2.y - p1.y) / (p2.x - p1.x)
        let b1 = p1.y - a1 * p1.x
        return (a1, b1)
    }
}
